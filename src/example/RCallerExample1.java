package example;

import java.io.File;
import java.io.IOException;

import rcaller.RCaller;
import rcaller.RCode;

public class RCallerExample1 {
	
	//http://stdioe.blogspot.fr/2011/07/rcaller-20-calling-r-from-java.html
	public static void main(String[] args) throws IOException {
		double[] numbers = new double[] {1,4,3,5,6,10};
		
		RCaller caller = new RCaller();
		caller.setRscriptExecutable("/usr/bin/Rscript");
		caller.cleanRCode();
		
		RCode code = caller.getRCode();
		
		code.addDoubleArray("x", numbers);
		
		File file = code.startPlot();
		code.addRCode("plot.ts(x)");
		code.endPlot();
		
		caller.runOnly();
		
		//ImageIcon ii= code.getPlot(file);
		
		code.showPlot(file);
	}
}
