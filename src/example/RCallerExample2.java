package example;

import java.io.File;
import java.io.IOException;

import rcaller.RCaller;

public class RCallerExample2 {

	public static void main(String[] args) throws IOException {
		// Creating an instance of class RCaller
		RCaller caller = new RCaller();

		// Important. Where is the Rscript?
		// This is Rscript.exe in windows
		caller.setRscriptExecutable("/usr/bin/Rscript");

		// Generating x and y vectors
		double[] x = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		double[] y = new double[] { 2, 4, 6, 8, 10, 12, 14, 16, 18, 30 };

		// Generating R code
		// addDoubleArray() method converts Java arrays to R vectors
		caller.getRCode().addDoubleArray("x", x);
		caller.getRCode().addDoubleArray("y", y);

		// ols<-lm(y~x) is totally R Code
		// but we send the x and the y vectors from Java
		caller.getRCode().addRCode("ols<-lm(y~x)");

		File file = caller.getRCode().startPlot();
		caller.getRCode().addRCode("plot(ols)");
		caller.getRCode().endPlot();

		// We are running the R code but
		// we want code to send some result to us (java)
		// We want to handle the ols object generated in R side
		//
		caller.runAndReturnResult("ols");

		// We are printing the content of ols
		System.out.println("Available results from lm() object:");
		System.out.println(caller.getParser().getNames());

		// Parsing some objects of lm()
		// Residuals, coefficients and fitted.values are some elements of lm()
		// object returned by the R. We parsing those elements to use in Java
		double[] residuals = caller.getParser().getAsDoubleArray("residuals");
		double[] coefficients = caller.getParser().getAsDoubleArray(
				"coefficients");
		double[] fitteds = caller.getParser().getAsDoubleArray("fitted_values");

		// Printing results
		System.out.println("Coefficients:");
		for (int i = 0; i < coefficients.length; i++) {
			System.out.printf("Beta %s = %s\n", i , coefficients[i]);
		}

		System.out.println("Residuals:");
		for (int i = 0; i < residuals.length; i++) {
			System.out.printf("%s = %s\n", i , residuals[i]);
		}
		
		System.out.println("fitteds:");
		for (int i = 0; i < fitteds.length; i++) {
			System.out.printf("%s = %s\n", i , fitteds[i]);
		}
		//show the graphic
		caller.getRCode().showPlot(file);
	}
}