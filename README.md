RCaller[rcaller] Example
===================

What is it ?
------------

An example of RCaller[rcaller] library. 

Before anything, install the R package "Runiversal" by typing

	install.packages ( "Runiversal" )


[rcaller](http://code.google.com/p/rcaller/)
